---
nome: Aldeia Pedra Branca
data: 2019-01-07T00:00:00.000Z
publicado: true
categorias:
  - aldeia
imagem: /images/aldeias/pedra_branca.jpg
coords:
  - "-47.637075"
  - "-8.301814"
descricao: >-
  Venda de produtos artesanais e regionais. Você encontra ervas do Cerrado, remédios naturais, ovo caipira, açúcar, rapadura, vinhos e licores de fruta. Além de artesanatos como tapetes, roupas e bolsas feitos em tear.

---

Mais...
