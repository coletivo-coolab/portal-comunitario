---
nome: Forno Velho
data: 2019-01-07T00:00:00.000Z
publicado: true
categorias:
  - aldeia
imagem:
coords:
  - "-47.563517"
  - "-8.477094"
descricao: >-
  Bar local que vende cerveja, cachaça e uma mesa de sinuca que você pode comprar as fichas pra jogar.
  Nesse mesmo local, no fundo tem a casa onde vive Edi, o proprietário do bar e sua esposa Dona Romana, que vende rosca, pães de queijo e serve um cafézinho e chá - tudo feito diariamente.

---

Mais informação...


