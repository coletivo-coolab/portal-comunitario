---
nome: Enciclopédia
data: 2020-09-07T00:00:00.000Z
publicado: true
categoria: Educação
imagem: /images/apps/kiwix.png
primeiro: false
link: 
apenasLink: false
cursoLink: https://www.kiwix.org
googlePlay: https://www.kiwix.org/en/kiwix-reader
appStore: https://www.kiwix.org/en/kiwix-reader
downloadAndroid: https://www.kiwix.org/en/kiwix-reader
downloadWin: https://www.kiwix.org/en/kiwix-reader
downloadWin32: https://www.kiwix.org/en/kiwix-reader
downloadMac: https://www.kiwix.org/en/kiwix-reader
downloadLinux: https://www.kiwix.org/en/kiwix-reader
---

Um aplicativo que leva conhecimento a milhões em todo o mundo, tornando-o disponível offline. Onde quer que você vá, você pode navegar na Wikipedia, ler livros da Biblioteca de Gutenberg ou assistir às palestras do TED e muito mais - mesmo se você não tiver uma conexão com a Internet.