---
nome: Emergência
data: 2020-09-07T00:00:00.000Z
publicado: true
categoria: Comunicação
imagem: /images/apps/meshtastic.webp
primeiro: false
link:
apenasLink: false
cursoLink: https://www.meshtastic.org/
googlePlay: https://play.google.com/store/apps/details?id=com.geeksville.mesh
appStore:
downloadAndroid: https://github.com/meshtastic/Meshtastic-Android/releases
downloadWin:
downloadWin32:
downloadMac:
downloadLinux:
---

Permite usar rádios baratos como comunicadores. Cada membro de sua rede pode ver a localização e distância de todos os outros membros e receber mensagens de texto enviadas para seu chat em um grupo privado.