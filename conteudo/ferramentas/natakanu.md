---
nome: Compartilhe arquivos
data: 2020-09-07T00:00:00.000Z
publicado: true
categoria: Compartilhamento
imagem: /images/apps/natakanu.gif
primeiro: false
link:
apenasLink: false
cursoLink: https://github.com/Wapikoni-Technique/Natakanu
googlePlay:
appStore:
downloadAndroid:
downloadWin: https://github.com/Wapikoni-Technique/Natakanu/releases
downloadWin32: https://github.com/Wapikoni-Technique/Natakanu/releases
downloadMac: https://github.com/Wapikoni-Technique/Natakanu/releases
downloadLinux: https://github.com/Wapikoni-Technique/Natakanu/releases
---

Uma ferramenta colaborativa que tem como objetivo aumentar a acessibilidade a ferramentas de compartilhamento, colaboração e transmissão de culturas indígenas, conhecimento e expressões artísticas. Funciona offline e online.