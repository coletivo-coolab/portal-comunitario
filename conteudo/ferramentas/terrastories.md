---
nome: Terrastories
data: 2020-09-07T00:00:00.000Z
publicado: true
categoria: Território
imagem: /images/apps/terrastories.png
primeiro: false
link: https://terrastories.io/demo/
apenasLink: false
cursoLink: https://terrastories.io/
googlePlay:
appStore:
downloadAndroid:
downloadWin:
downloadWin32:
downloadMac:
downloadLinux:
---

Um aplicativo de geo-narrativa desenvolvido para permitir que comunidades indígenas e outras comunidades locais localizem e mapeiem suas próprias tradições de narração oral sobre lugares de significado ou valor significativo para eles. Funciona offline, para que comunidades remotas possam acessar o aplicativo sem necessidade de internet.