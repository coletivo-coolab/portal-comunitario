---
nome: Repositório de Cultura
data: 2020-09-07T00:00:00.000Z
publicado: true
categoria: Cultura
imagem: /images/apps/ahau.png
primeiro: false
link:
apenasLink: false
cursoLink: https://ahau.io
googlePlay:
appStore:
downloadAndroid: https://github.com/protozoa-nz/whakapapa-ora/releases
downloadWin: https://github.com/protozoa-nz/whakapapa-ora/releases
downloadWin32: https://github.com/protozoa-nz/whakapapa-ora/releases
downloadMac: https://github.com/protozoa-nz/whakapapa-ora/releases
downloadLinux: https://github.com/protozoa-nz/whakapapa-ora/releases
---

Uma plataforma de dados que ajuda comunidades auto-organizadas a capturar, preservar e compartilhar informações e histórias em servidores e bancos de dados auto-gerenciados. Permite trabalhar com árvores genealógicas, registros e repositórios culturais, respeitando a soberania de dados do povo.