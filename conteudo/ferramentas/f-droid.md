---
nome: Aplicativos para celular
data: 2020-09-07T00:00:00.000Z
publicado: true
categoria: Compartilhamento
imagem: /images/apps/f-droid.png
primeiro: false
link:
apenasLink: false
cursoLink: https://f-droid.org/
googlePlay:
appStore:
downloadAndroid: https://f-droid.org/F-Droid.apk
downloadWin:
downloadWin32:
downloadMac:
downloadLinux:
---

Um catálogo instalável de softwares de código livre e aberto para a plataforma Android. O aplicativo facilita a busca, instalação e compartilhamento mesmo offline de aplicativos, além de checar novas atualizações.