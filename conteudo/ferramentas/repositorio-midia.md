---
nome: Repositório de Mídia
data: 2020-09-07T00:00:00.000Z
publicado: true
categoria: Cultura
imagem: /images/apps/flix.jpg
primeiro: false
link: https://demo.jellyfin.org
apenasLink: true
cursoLink:
googlePlay:
appStore:
downloadAndroid:
downloadWin:
downloadWin32:
downloadMac:
downloadLinux:
---

Séries, documentários, filmes, além de várias opções musicais curados por nós da comunidade.
