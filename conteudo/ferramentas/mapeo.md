---
nome: Mapeo
data: 2020-09-07T00:00:00.000Z
publicado: true
categoria: Território
imagem: /images/apps/mapeo.png
primeiro: false
link:
apenasLink: false
cursoLink: https://www.digital-democracy.org/mapeo/
googlePlay: https://play.google.com/store/apps/details?id=com.mapeo
appStore:
downloadAndroid: https://github.com/digidem/mapeo-mobile/releases
downloadWin: https://github.com/digidem/mapeo-desktop/releases
downloadWin32: https://github.com/digidem/mapeo-desktop/releases
downloadMac: https://github.com/digidem/mapeo-desktop/releases
downloadLinux: https://github.com/digidem/mapeo-desktop/releases
---

Um poderoso conjunto de ferramentas de mapeamento para as comunidades. Mapeo é uma ferramenta digital gratuita para documentar, monitorar e mapear, possibilitando a troca de dados fora da Internet, de aparelho para aparelho, de forma segura.