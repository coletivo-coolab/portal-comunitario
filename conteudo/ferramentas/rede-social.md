---
nome: Rede Social
data: 2020-09-07T00:00:00.000Z
publicado: true
categoria: Comunicação
imagem: /images/apps/manyverse.png
primeiro: true
link: https://feedless.social/
apenasLink: false
cursoLink:
googlePlay: https://play.google.com/store/apps/details?id=se.manyver
appStore: https://apps.apple.com/app/manyverse/id1492321617
downloadAndroid: http://manyver.se/apk
downloadWin: https://github.com/ssbc/patchwork/releases
downloadWin32: https://github.com/ssbc/patchwork/releases
downloadMac: https://github.com/ssbc/patchwork/releases
downloadLinux: https://github.com/ssbc/patchwork/releases
---

Rede social que funciona como outros aplicativos sociais que já usamos. A principal diferença é que ele funciona mesmo sem internet por nossa rede local.
