---
nome: Cursos
data: 2020-09-07T00:00:00.000Z
publicado: true
categoria: Educação
imagem: /images/apps/kolibri.jpg
primeiro: false
link: https://learningequality.org/kolibri/
apenasLink: true
cursoLink:
googlePlay:
appStore:
downloadAndroid:
downloadWin:
downloadWin32:
downloadMac:
downloadLinux:
---

Uma plataforma de cursos online que disponibiliza uma tecnologia educacional de alta qualidade.
