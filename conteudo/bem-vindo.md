---
titulo: Aldeia Serra Grande
comoUsarLink: https://gitlab.com/coletivo-coolab/servidor-comunitario
imagens:
  - /images/bem-vindo/Serra_Grande.jpg
  - /images/bem-vindo/servidor_comunitario.jpg
  - /images/bem-vindo/servidor_comunitario_caixa.jpg
  - /images/bem-vindo/portal_comunitario_pulseira.jpg
---

# Hapa! Ajpen me ju run ijakri

Esse portal foi criado para disponibilizar ferramentas de comunicação e informação para aldeias, mesmo que sem acesso a Internet, para que tenham um espaço digital próprio.

Aqui estão disponíveis ferramentas com o propósito de fortalecer o processo de defesa e expansão da Terra Indígena (pjê), trazer formação técnica para os jovens (mentu) ao mesmo tempo que buscando valorizar a cultura Krahô.