# Portal Comunitário

## Instalação
Vai precisar ter NodeJS maior que versão 12 instalado. Se não tiver use o [NVM](https://github.com/nvm-sh/nvm) para instalar.

## Uso

`git clone https://gitlab.com/coletivo-coolab/portal-comunitario.git`
`npm i`
`npm run dev`

## Build
`npm run build`

Depois de fazer o build copie a pasta `dist` para onde vai rodar seu projeto ou use o Docker.


## TODO:
- Adicionar script para atualizar geojson do [https://native-land.ca/](https://native-land.ca/)
- Melhor interação em clique da camada